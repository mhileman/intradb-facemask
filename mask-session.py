import os
import sys
import shutil
# import time
import threading

from pyxnat import Interface

sys.path.append("/nrgpackages/tools.release/intradb")

from IntradbPipeline.params import parser
from IntradbPipeline.common import Pipeline
from hcpxnat.interface import HcpInterface

'''
DICOM to NIFTI specific methods for Intradb.
Set up optparse and HcpInterface first, then use them to instantiate Pipeline.
'''

# Add params specific to this pipeline here
# Other parameters are pulled from the generic params.py
parser.add_option("-r", "--ref-session", action="store", type="string",
                  dest="ref_session", default="none", help='Reference ' +
                  'session containing scan to co-register. Default none.')
parser.add_option("-R", "--ref-scan", action="store", type="string",
                  dest="ref_scan", default="none", help='Reference ' +
                  'scan used to co-register against Atlas. Default none.')
parser.add_option("-B", "--use-bet", action="store", type="string",
                  dest="use_bet", default="1", help="If set to 1, " +
                  "use FSL's BET to exclude brain mask. Default 1.")
parser.add_option("-i", "--invasiveness", action="store", type="string",
                  dest="invasiveness", default="1.0", help='Invasiveness or ' +
                  'grid step coefficient, ranging from 0.1-3.0. Default 1.0.')
parser.add_option("-t", "--threshold", action="store", type="string",
                  dest="threshold", default="-1", help='Mask threshold. ' +
                  'float, Default -1.')

# TODO Pull reference scans down in threads while attempting to run with self.
# Make a list of references to try, self, ref1, ref2, etc.
(opts, args) = parser.parse_args()

# PyXNAT interface module
py_xnat = Interface(
    server=opts.hostname,
    user=opts.alias,
    password=opts.secret
)
# Disable SSL verification, won't work on dev instances otherwise
py_xnat._http.verify = False

# Home grown Python-XNAT interface
xnat = HcpInterface(
    url=opts.hostname,
    username=opts.alias,
    password=opts.secret
)
xnat.project = opts.project
xnat.session_label = opts.session
# Take an optional subject label, otherwise find the subject label
if not opts.subject:
    xnat.subject_label = xnat.getSessionSubject()

# Setup Pipeline object to use common pipeline functionality
pipe = Pipeline(xnat, opts, name='fm')

pipe.log.debug("BUILD DIR after pipe initialization -- {}".format(pipe.build_dir))


def main():
    scans = getDefaceScans()

    if opts.ref_session == 'none':
        opts.ref_session = opts.session

    if opts.ref_scan == 'auto':
        opts.ref_scan = findReferenceScan()

    if opts.ref_scan != 'none':
        downloadReferenceScan()

    runFacemask(scans)
    pipe.combineScanLogs()
    #shutil.rmtree(pipe.build_dir)


def getDefaceScans():
    deface_types = ['T1w', 'T2w']
    scan_ids = set()
    skip_ids = set()

    # Process only user provided scans if any
    scans_param = opts.scans.split(',') if opts.scans else []
    # Allow 'all' for param for handling by XNAT automation service
    if 'all' in scans_param:
        scans_param.remove('all')

    for scan in xnat.getSessionScans():
        xnat.scan_id = scan['ID']

        # Handle user supplied scan list
        if scans_param and xnat.scan_id not in scans_param:
            continue

        for st in deface_types:
            sd = scan['series_description']
            # Continue if it's not a deface type or it is a setter
            if st not in sd or 'setter' in sd:
                continue

            scan_ids.add(scan['ID'])

            # Check scan resources for existing defaced scans
            resources = xnat.getScanResources()

            # Fail or skip if not set to overwrite
            # Overwriting of Defaced DICOM happens in mask-scan.sh
            for r in resources:
                if r['label'] == 'DICOM_ORIG' and opts.existing == 'fail':
                    s = scan['ID']
                    pipe.log.warn('Aborting Facemasking. "fail" on existing')
                    pipe.log.warn('Scan {} has DICOM_ORIG resource'.format(s))
                    sys.exit(1)
                elif r['label'] == 'DICOM_ORIG' and opts.existing == 'skip':
                    skip_ids.add(xnat.scan_id)

    if skip_ids:
        pipe.log.info("Skipping scans with DICOM_ORIG {}".format(
            ','.join(skip_ids)))

    # Remove skipped scans and return as a list
    scan_list = list(scan_ids - skip_ids)
    pipe.log.info("Masking scans {}".format(','.join(scan_list)))

    if not scan_list:
        pipe.log.info("No faces to mask. Exiting ...")
        sys.exit(0)

    return scan_list


def downloadReferenceScan():

    #if opts.ref_scan == 'auto':
    #    opts.ref_scan = findReferenceScan()
    #else:
    #    ref = opts.ref_scan

    pipe.log.info("Downloading reference scan {} from session {}".format(
        opts.ref_scan, opts.ref_session))

    ref = py_xnat.select \
        .project(opts.project) \
        .subject(xnat.subject_label) \
        .experiment(opts.ref_session) \
        .scan(opts.ref_scan)

    # Assume we're using DICOM resource unless there is a DICOM_ORIG
    dicom_label = 'DICOM'

    for resource in ref.resources():
        if resource.label() == 'DICOM_ORIG':
            dicom_label = 'DICOM_ORIG'

    dicom_resource = ref.resource(dicom_label)

    t = threading.Thread(
        name='getRefScan',
        target=downloadWorker,
        args=(dicom_resource, dicom_label,)
    )
    t.start()
    t.join()

    pipe.log.info("Finished downloading reference scan")


def downloadWorker(dicom_resource, dicom_label):
    #pipe.log.debug("BUILD DIR inside downloadWorker() -- {}".format(pipe.build_dir))
    #ref_dir = os.path.join(pipe.build_dir, 'reference')
    #pipe.log.debug("REF_DIR inside downloadWorker() -- {}".format(ref_dir))
    #pipe.mkdir_p(ref_dir)

    # mask_face script assumes reference is in root directory 

    dicom_resource.get(pipe.build_dir, extract=True)
    src = os.path.join(pipe.build_dir, dicom_label)
    dst = os.path.join(pipe.build_dir, opts.ref_scan)
    os.rename(src, dst)


def findReferenceScan():
    # Get the reference scan from whatever session it is in
    session = py_xnat.select \
        .project(opts.project) \
        .subject(xnat.subject_label) \
        .experiment(opts.ref_session)

    for scan_id in session.scans().get():
        scan_type = session.scan(scan_id).attrs.get('type')

        #if 'T1w_Norm_4e' in scan_type:
        if scan_type == 'T1w':
            return scan_id


def runFacemask(scans):
    """
    Build a mask_face command for each scan and pass to the Pipeline to execute
    """
    pwd = os.path.dirname(os.path.realpath(__file__))
    script_path = os.path.join(pwd, 'mask-scan.sh')
    # facemask_dir = '/nrgpackages/tools.release/facemasking-20141020'
    mask_face_dir = '/nrgpackages/tools.release/intradb/facemask/mask_face'
    threads = []

    pipe.setPcpStatus('FaceMasking', 'QUEUED')

    for scan in scans:
        xnat.scan_id = scan
        # Figure out whether we're operating on DICOM or DICOM_ORIG
        resources = xnat.getScanResources()
        resource_label = 'DICOM'
        # We can assume if we're defacing this scan and there's DICOM_ORIG,
        # that's what we need to operate on
        for r in resources:
            if r['label'] == 'DICOM_ORIG':
                resource_label = 'DICOM_ORIG'

        # Set reference scan to self if none is specified
        # if opts.ref_session == 'none':
        #     opts.ref_session = opts.session
        # if opts.ref_scan = 'none':
        #     opts.ref_scan = scan

        pipe.log.debug("BUILD DIR inside runFacemask() -- {}".format(pipe.build_dir))

        command = "{} {} {} {} {} {} {} {} {} {} {} {} {} {} {}".format(
            script_path,
            xnat.username,
            xnat.password,
            xnat.url,
            xnat.project,
            xnat.subject_label,
            xnat.session_label,
            scan,
            resource_label,
            opts.ref_scan,
            pipe.build_dir,
            mask_face_dir,
            opts.use_bet,
            opts.invasiveness,
            opts.threshold
        )
        t = threading.Thread(
            name=scan,
            target=pipe.submit,
            args=(scan, command, '', 'hcp_priority.q')
            #args=(scan, command, '', 'hcp_standard.q')
        )
        threads.append(t)
        t.start()

    # Wait for all threads to finish
    for t in threads:
        t.join()

    pipe.setPcpEndStatus('FaceMasking')


if __name__ == "__main__":
    main()
